
<!-- README.md is generated from README.Rmd. Please edit that file -->

# DEGAStools

<!-- badges: start -->

<!-- badges: end -->

DEGAStools is a package to provide basic functionality for reading,
analyzing and plotting data output of the DEGAS instrument at FSU Jena.
Currently the data input is optimized for the QMS .sac files returned by
that specific system.

DEGAS is a ‘Direct-coupled Evolved Gas Analysis System’ and uses a
quadrupole mass spectrometer to detect gaseous species emitted in
high-vacuum from a heated sample (up to 1450 °C). Mass loss can be
monitored via a thermogravimetric analyzer.

## Installation

You can install the released version of DEGAStools with:

``` r
library(devtools)
devtools::install_gitlab("dennisharries/DEGAStools")
```

or

``` r
library(remotes)
remotes::install_gitlab("dennisharries/DEGAStools")
```

## Example

This is a basic example which shows you how to read and pre-process
data:

``` r
library(DEGAStools)

setwd("/your/working/directory")

DEGASobj <- readDEGAS("degas_data.sac")
DEGASobj <- addTGA(DEGASobj, "tga_data.txt")
DEGASobj <- calibDEGAS(DEGASobj, cutoff = 7e-11)
DEGASobj <- quantDEGAS(DEGASobj, delta = 0.4)
```

There is also a wrapping function to do this in one step (recommended):

``` r
library(DEGAStools)

setwd("/your/working/directory")

DEGASobj <- processDEGAS("degas_data.sac", "tga_data.txt")
```

Comparing sample and blank measurements

``` r
library(DEGAStools)

plotMassSpec(MUR_1, BLK_1, Temp1 = 200)
```

<img src="man/figures/README-example3-1.png" width="100%" />

Release profiles as function of temperature

``` r
library(DEGAStools)

plotTempSeries(DEGASobj1 = MUR_1,
               masses = c(18, 28, 44),
               log = "",
               xlim = c(0, 1460),
               ylim = c(0, 1e-9),
               massloss = TRUE,
               norm = "massrate")
```

<img src="man/figures/README-example4-1.png" width="100%" />
