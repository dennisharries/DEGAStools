#' Blank DEGAS run at a heating rate of 10 K/min up to 1450 °C
#'
#'
#' @format a DEGASobj list with 29 entries
#'  - *degasName*: Name of DEGAS .sac raw file
#'  - *tgaName*: Name of TGA .txt raw file
#'  - *tgaSampleMass*: Sample mass in mg (0.1 as dummy value for blank)
"BLK_1"
