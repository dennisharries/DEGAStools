#' Bundled process flow for DEGAS analysis
#'
#' The function reads the input files, calibrates the m/z ratios of the mass
#' spectrum, and quantifies the peak ion currents.
#' @param sacFile name of the file containing the mass spectrometric data. These
#' are .sac files. Use the full path if the file is not in the working
#' directory.
#' @param tgaFile name of the data file containing the thermogravimetric data.
#' Typically this is a .txt file. Use the full path if the file is not in the
#' working directory.
#' @param cutoff minimum peak ion current in Ampere for the peak search
#' algorithm.
#' @param delta half-width of the peak search region for a given integral m/z.
#'
#' @return A DEGASobj list containing the raw data, the meta data, and the peak
#' ion currents.
#' @export
#'
#' @examples
processDEGAS <- function(sacFile, tgaFile, cutoff = 7e-11, delta = 0.4) {
  DEGASobj <- readDEGAS(sacFile)
  DEGASobj <- addTGA(DEGASobj, tgaFile)
  DEGASobj <- calibDEGAS(DEGASobj, cutoff = cutoff)
  DEGASobj <- quantDEGAS(DEGASobj, delta = delta)
  return(DEGASobj)
}

################################################################################

#' Read mass spectrometric data
#'
#' Reads the mass spectrometric output stored in .sac files.
#' @param sacFile name of the file containing the mass spectrometric data. These
#' are .sac files. Use the full path if the file is not in the working
#' directory.
#'
#' @return A DEGASobj list
#' @export
#'
#' @examples
readDEGAS <- function(sacFile) {
  ## open file connection
  fid <- file(sacFile, "rb")

  ## number of cycles
  seek(fid, 100, "start")
  numberCycles <- readBin(
    fid,
    "integer",
    n = 1L,
    size = 2,
    signed = FALSE,
    endian = "little"
  )

  ## length of data cycle bytes
  seek(fid, 106, "start")
  lengthCycle <- readBin(
    fid,
    "integer",
    n = 1L,
    size = 2,
    signed = FALSE,
    endian = "little"
  )

  ## time when storage was started
  seek(fid, 194, "start")
  startDate0 <- readBin(
    fid,
    n = 1L,
    what = "integer",
    size = 4,
    endian = "little"
  )

  startDate <- as.POSIXct(
    startDate0,
    origin = "1970-01-01 00:00:00 UTC",
    tz = "UTC"
  )

  ## length of header bytes
  seek(fid, 205, "start")
  headerBytes <- readBin(
    fid,
    "integer",
    n = 1L,
    size = 2,
    signed = FALSE,
    endian = "little"
  )

  ## scan width (mass numbers)
  seek(fid, 354, "start")
  scanWidth <- readBin(
    fid,
    "integer",
    n = 1L,
    size = 1,
    signed = FALSE,
    endian = "little"
  )

  ## pointsPerMass
  seek(fid, 356, "start")
  pointsPerMass <- readBin(
    fid,
    "integer",
    n = 1L,
    size = 1,
    signed = FALSE,
    endian = "little"
  )

  rawScale <- seq(1 / pointsPerMass, scanWidth, 1 / pointsPerMass)

  ## ion current data (Ampere)
  seek(fid, headerBytes, "start")
  ionCurrents <- readBin(
    fid,
    "numeric",
    n = numberCycles * lengthCycle,
    size = 4,
    endian = "little"
  )

  ionCurrents = matrix(
    ionCurrents,
    nrow = numberCycles,
    byrow = TRUE
  )[, -c(1, 2, 3, 2244)]

  ## cycle header, time information (seconds since start)
  seek(fid, headerBytes, "start")
  cycleHeader <- readBin(
    fid,
    "integer",
    n = numberCycles * lengthCycle,
    size = 4,
    endian = "little"
  )

  cycleHeader1 <- matrix(
    cycleHeader,
    nrow = numberCycles,
    byrow = TRUE
  )[, 1]

  cycleTime <- as.POSIXct(
    startDate0 + cycleHeader1,
    origin = "1970-01-01 00:00:00 UTC",
    tz = "UTC"
  )

  ## close file connection
  close(fid)

  DEGASobj <- list(
    degasName = sacFile,
    degasHeaderBytes = headerBytes,
    degasNumberCycles = numberCycles,
    degasLengthCycle = lengthCycle,
    degasStartDate = startDate,
    degasScanWidth = scanWidth,
    degasPointsPerMass = pointsPerMass,
    degasRawScale = rawScale,
    degasCycleTime = cycleTime,
    degasIonCurrents = ionCurrents
  )

  return(DEGASobj)
}

################################################################################

#' Read thermogravimetric data
#'
#' Reads the thermogravimetric output stored in .txt files.
#' @param tgaFile name of the file containing the thermogravimetric data. These
#' are .txt files. Use the full path if the file is not in the working
#' directory.
#' @param DEGASobj a DEGASobj list to append the data to, currently this is
#' required.
#'
#' @return  A DEGASobj list
#' @export
#'
#' @examples
addTGA <- function(DEGASobj, tgaFile) {
  ## read TGA data from file
  tgaData <- read.table(tgaFile, sep = ";")[, 1:4]
  names(tgaData) <- c("T_C", "t_min", "mass", "vac")

  ## metadata
  headerData <- readLines(tgaFile, n = 60L)
  headerData <- headerData[1:(grep("##Temp", headerData) - 2)]

  tgaSampleMass <- as.numeric(
    strsplit(headerData[grep("SAMPLE MASS", headerData)], ":")[[1]][2]
  )

  tgaCrucibleMass = as.numeric(
    strsplit(headerData[grep("SAMPLE CRUCIBLE MASS", headerData)], ":")[[1]][2]
  )

  ## metadata start time
  tgaStart <- as.POSIXct(
    substr(headerData[grep("DATE/TIME", headerData)], 12, 27),
    "%d.%m.%Y %H:%M",
    tz = "CET"
  )

  attr(tgaStart, "tzone") <- "UTC"

  ## Time in tga file is reported with 1 min precision,
  ## but precise time is found in the the DEGAS file
  deltaTime <- 0
  matchingIndex <- which(DEGASobj$degasCycleTime >= tgaStart + deltaTime)[1]
  tgaStartPrecise <- DEGASobj$degasCycleTime[matchingIndex]

  ## tga relative time stamps in seconds since start
  tgaRelTime <- tgaData[, 2] * 60
  tgaAbsTime <- tgaStartPrecise + tgaRelTime

  ## smooth TGA temperature record, which sometimes shows oscillations
  tgaTempRaw <- tgaData[,"T_C"]
  ssp <- smooth.spline(tgaRelTime, tgaTempRaw, spar = 0.4)
  tgaTemp <- ssp$y

  ## calculate the heating rate for every measured TGA step (K/s)
  timeTempFun <- splinefun(tgaRelTime, tgaTemp + 273.15)
  tgaRate <- timeTempFun(tgaRelTime, deriv = 1)
  tgaRateMedian <- median(tgaRate)

  ## interpolation functions for TGA measurements
  tTemp_linear <- approxfun(x = tgaAbsTime, y = tgaTemp)
  tMass_linear <- approxfun(x = tgaAbsTime, y = tgaData[, 3])
  tVac_linear <- approxfun(x = tgaAbsTime, y = tgaData[, 4])

  # interpolation to get values at DEGAS cycles times
  degasTemp <- tTemp_linear(DEGASobj$degasCycleTime)
  degasRelMass <- tMass_linear(DEGASobj$degasCycleTime)
  degasVac <- tVac_linear(DEGASobj$degasCycleTime)


  TGAobj <- list(
    tgaHeader = headerData,
    tgaName = tgaFile,
    tgaSampleMass = tgaSampleMass,
    tgaCrucibleMass = tgaCrucibleMass,
    tgaStartTime = tgaStart,
    tgaRelTime = tgaRelTime,
    tgaAbsTime = tgaAbsTime,
    tgaTempRaw = tgaTempRaw,
    tgaTemp = tgaTemp,
    tgaRate = tgaRate,
    tgaRateMedian = tgaRateMedian,
    tgaRelMass = tgaData[, 3],
    tgaVac = tgaData[, 4],
    degasTemp = degasTemp,
    degasRelMass = degasRelMass,
    degasVac = degasVac
  )

  DEGASobj <- c(DEGASobj, TGAobj)
  return(DEGASobj)
}

################################################################################

#' Calibration of m/z scale
#'
#' Calibrates the m/z scale based on a heuristic approach for the DEGAS
#' quadrupole mass spectrumeter currently installed at FSU Jena.
#' @param DEGASobj a DEGASobj list containing the mass spectrometric and
#' thermogravimetric raw data.
#' @param cutoff minimum peak ion current in Ampere for the peak search
#' algorithm.
#' @param switch m/z ratio at which the raw m/z scale and the true m/z scale
#' coincide.
#'
#' @return a DEGASobj list with the calibration data appended.
#' @export
#'
#' @examples
calibDEGAS <- function(DEGASobj, cutoff = 7e-11, switch = 39){

  data <- DEGASobj$degasIonCurrents
  rawScale <- DEGASobj$degasRawScale

  ## assumes scan starts at m = 1 / pointsPerMass
  cumData = colSums(data)

  peaks <- pracma::findpeaks(
    cumData,
    minpeakdistance = 28,
    minpeakheight = cutoff
  )

  peaks <- cbind(peaks, rawScale[peaks[, 2]])
  peaks <- peaks[order(peaks[, 5]), ]

  peakInd <- peaks[, 2]
  rawMass <- peaks[, 5]
  floorDelta <- (rawMass - floor(rawMass))

  calMass <- c(
    floor(rawMass[which(rawMass <= switch)]),
    ceiling(rawMass[which(rawMass > switch)])
  )

  fit <- lm(calMass ~ peakInd)
  specInd <- seq(1:ncol(data))
  calScale <- specInd * fit$coeff[2] + fit$coeff[1]

  ## diagnostic plots
  # par(mfrow = c(2, 1))
  # plot(calScale, cumData, type = "l", log = "y")
  # lines(rawScale, cumData, col = "red")
  # points(x = peaks[, 5], y = peaks[, 1], col = "red")
  # abline(h = cutoff, lty = 2)

  # plot(peakInd, calMass)
  # points(peakInd, rawMass, col = "red")
  # abline(a = fit$coeff[1], b = fit$coeff[2])

  CALobj <- list(degasCalScale = calScale)

  DEGASobj <- c(DEGASobj, CALobj)
  return(DEGASobj)
}

################################################################################

#' Internal function for peak processing
#'
#' @param x data section to search for a peak (width = 2 * delta)
#'
#' @return
#' @export
#'
#' @examples
.getCurr <- function(x) {

  peakPos <- pracma::findpeaks(
    x,
    nups = 2,
    ndowns = 1,
    zero = "+",
    sortstr = TRUE
  )

  if (is.null(peakPos)) {
    ## no peak found (e.g. shoulder), calculate a approx. mean ion current.
    y <- exp(mean(log(x)))
  } else {
    ## one peak found, returns peak ion current.
    if (nrow(peakPos) == 1) {
      y <- peakPos[1, 1]
    } else {
      ## multiple peaks found,  return mean of two largest ion currents.
      if (nrow(peakPos) > 1) {
        y <- mean(peakPos[1:2, 1])
      }
    }
  }

  return(y)
}

################################################################################

#' Internal function for peak processing
#'
#' @param DEGASobj a DEGASobj list.
#' @param slice time slice of mass spectrometric data, i.e., a single spectrum.
#' @param delta half-width of the peak search region for a given integral m/z.
#'
#' @return
#' @export
#'
#' @examples
.getCurrents <- function(DEGASobj, slice, delta = 0.4) {
  spectrumMass <- DEGASobj$degasCalScale
  spectrumCurr <- DEGASobj$degasIonCurrent[slice, ]

  ## Sometimes (always?) there is a m/z = 0, or even a m/z = -1 entry.
  ## tail() only takes the last (n = degasScanWidth) m/z from the list
  s <- ceiling(spectrumMass - delta) + floor(spectrumMass + delta) + 1

  # massList <- tail(
  #  split(spectrumMass, ceiling(spectrumMass - delta) * s %% 2),
  #  DEGASobj$degasScanWidth
  # )

  currList <- tail(
    split(spectrumCurr, ceiling(spectrumMass - delta) * s %% 2),
    DEGASobj$degasScanWidth
  )

  return(
    unlist(
      lapply(currList, .getCurr)
    )
  )
}

################################################################################

#' Quantification of ion currents at peak maximum
#'
#' @param DEGASobj a DEGASobj list.
#' @param delta half-width of the peak search region for a given integral m/z.
#'
#' @return
#' @export
#'
#' @examples
quantDEGAS <- function(DEGASobj, delta = 0.4) {
  startTime <- Sys.time()

  N <- DEGASobj$degasNumberCycles
  M <- DEGASobj$degasScanWidth
  peakIonCurrents <- matrix(nrow = N, ncol = M)
  peakMassNumbers <- seq(1, M)

  for (n in 1:N) {
    i <- .getCurrents(DEGASobj = DEGASobj, slice = n, delta = delta)
    peakIonCurrents[n, ] <- i

    message(
      paste("Please wait, processing cycle",n , "of", N),
      "\r",
      appendLF = FALSE
    )

    flush.console()
  }

  quant <- list(
    degasPeakIonCurrents = peakIonCurrents,
    degasPeakMassNumber = peakMassNumbers)
  DEGASobj <- c(DEGASobj, quant)

  ## total time elapsed
  endTime <- Sys.time()
  deltaTime <- difftime(endTime, startTime, units = "secs")
  t1 <- round(deltaTime, 3)

  ## time per cycle
  t2 <- round(deltaTime * 1000 / N, 0)

  message(
    paste("Processing took", t1, "s,", t2, "ms per quadrupole cycle"),
    "\r"
  )

  flush.console()

  return(DEGASobj)
}

################################################################################

#' Export DEGAS data to csv
#'
#' @param DEGASobj a DEGASobj list to be exported after full processing
#' @param what either "peak" for peak ion currents or "raw" for raw data
#'
#' @return a .csv file saved to the working directory
#' @export
#'
#' @examples
exportDEGAS <- function(DEGASobj, what = "peak") {
  if (what == "peak") {
    data <- as.data.frame(round(DEGASobj$degasTemp, 3))
    data <- cbind(data, DEGASobj$degasPeakIonCurrents)
    data <- cbind(data, DEGASobj$degasRelMass)
    data <- cbind(data, DEGASobj$degasCycleTime)
    massNames <- paste("m/z", DEGASobj$degasPeakMassNumber)

    colnames(data) <- c("temp_C",
                        massNames,
                        "rel_mass_loss",
                        "time")

    baseName <- gsub(".sac", "", DEGASobj$degasName)
    fileName <- paste(baseName, "_", what, ".csv", sep = "")
    write.csv(data, file = fileName, row.names = FALSE)
  } else {
    if (what == "raw") {
      data <- as.data.frame(round(DEGASobj$degasTemp, 3))
      data <- cbind(data, DEGASobj$degasIonCurrents)
      data <- cbind(data, DEGASobj$degasRelMass)
      data <- cbind(data, DEGASobj$degasCycleTime)

      colnames(data) <- c("temp_C",
                          DEGASobj$degasCalScale,
                          "rel_mass_loss",
                          "time")

      baseName <- gsub(".sac", "", DEGASobj$degasName)
      fileName <- paste(baseName, "_", what, ".csv", sep = "")
      write.csv(data, file = fileName, row.names = FALSE)
    }
  }
}
