% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DEGASmultivar.R
\name{plotPCA}
\alias{plotPCA}
\title{Plot a principal component analysis of DEGAS data.}
\usage{
plotPCA(DEGASobj, labelThreshold = 0, scaling = FALSE)
}
\arguments{
\item{DEGASobj}{a DEGASobj list after current quantification.}

\item{labelThreshold}{threshold to control which m/z labels are shown in the
plot. Defaults to 0 (all labels shown).}

\item{scaling}{logical, if TRUE scaling in prcomp() is applied.}
}
\value{
a biplot.
}
\description{
Plot a principal component analysis of DEGAS data.
}
